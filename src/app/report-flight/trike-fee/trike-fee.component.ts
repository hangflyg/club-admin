import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, ValidatorFn, ValidationErrors, FormBuilder } from '@angular/forms';
import { Person, Flight } from 'src/app/models';
import { CreateRequestAction } from 'src/app/crud-store';
import { transactionsStore } from 'src/app/app-store-model';
import { Store } from '@ngrx/store';
import { FlightTypes } from 'src/app/flight-types.enum';

@Component({
  selector: 'sdfk-trike-fee',
  templateUrl: './trike-fee.component.html',
  styleUrls: ['./trike-fee.component.scss']
})
export class TrikeFeeComponent implements OnInit {
  @Input() price: number;
  @Input() form: FormGroup;
  @Input() people: Person[];

  static generateForm(fb: FormBuilder) {
    return fb.group({
      trikePilotPay: true,    // true === the trike pilot pays for 'friflyg'
      billingPerson: null,    // the person paying for 'friflyg'
    }, { validators: [feeValidator] });
  }

  constructor() { }

  ngOnInit() { }

  chargeFlight(newFlight: Flight): CreateRequestAction[] {
    const transData = {
      transactionId: null,
      licNr: this.form.value.trikePilotPay ? newFlight.licNr : this.form.value.billingPerson.licNr,
      flightId: newFlight.flightId,
      date: newFlight.date,
      height: 0,
      dayFee: 0,
      price: this.price,
      type: FlightTypes.trikeflygningPrivat,
      note: newFlight.note
    };
    const transactionAction = transactionsStore.createRequestAction(transData);
    return [transactionAction];
  }
}

export const feeValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  let error = null;
  const value = control.value;
  if (!value.trikePilotPay && (!control.value.billingPerson || (typeof control.value.billingPerson === 'string'))) {
    error = {trikePilotPay: 'välj vem som betalar'};
    control.get('billingPerson').setErrors({person: 'välj vem som betalar'});
  }
  return error;
};
