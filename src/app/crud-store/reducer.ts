import { initialState, CrudState } from './state';
import * as Crud from './actions';
import { createEntityAdapter } from '@ngrx/entity';

export function crudReducer(state = initialState, action: Crud.ActionsUnion): CrudState {
  switch (action.type) {
    case Crud.ActionTypes.SET_CONFIG: {
      const config = action.payload;
      const entities = {};
      Object.keys(config).forEach(key => {
        const adapter = createEntityAdapter<any>(config[key].adapterOptions);
        const entityState = adapter.getInitialState();
        entities[key] = { adapter, entityState };
      });
      return {
        ...state,
        config,
        entities
      };
    }

    case Crud.ActionTypes.CREATE_REQUEST:
    case Crud.ActionTypes.DELETE_REQUEST:
    case Crud.ActionTypes.LOAD_REQUEST: {
      return {
        ...state,
        requestCount: state.requestCount + 1,
      };
    }

    case Crud.ActionTypes.CREATE_FAILURE:
    case Crud.ActionTypes.DELETE_FAILURE:
    case Crud.ActionTypes.LOAD_FAILURE: {
      return {
        ...state,
        requestCount: state.requestCount - 1,
        error: action.payload.error
      };
    }

    case Crud.ActionTypes.LOAD_SUCCESS: {
      const name = action.payload.name;
      const adapter = state.entities[name].adapter;
      const entityState = adapter.addAll(action.payload.data, adapter.getInitialState());
      return {
        ...state,
        requestCount: state.requestCount - 1,
        entities: {...state.entities, [name]: { adapter, entityState }}
      };
    }

    case Crud.ActionTypes.CREATE_SUCCESS: {
      const name = action.payload.name;
      const entity = state.entities[name];
      const adapter = entity.adapter;
      const oldState = entity.entityState;
// TODO remove type conversion on line below
      const newState = adapter.addOne(action.payload.entity, oldState);
      return {
        ...state,
        requestCount: state.requestCount - 1,
        entities: {...state.entities, [name]: { adapter, entityState: newState }}
      };
    }

    case Crud.ActionTypes.DELETE_SUCCESS: {
      const name = action.payload.name;
      const entity = state.entities[name];
      const adapter = entity.adapter;
      const oldState = entity.entityState;
// TODO remove type conversion on line below
      const newState = adapter.removeOne(adapter.selectId(action.payload.entity) as number, oldState);
      return {
        ...state,
        requestCount: state.requestCount - 1,
        entities: {...state.entities, [name]: { adapter, entityState: newState }}
      };
    }

    default: {
      return state;
    }
  }
}
