import { DeleteRequestAction, CreateRequestAction, ActionTypes, ActionsUnion, CreateSucccessAction, LoadRequestAction } from './actions';
import { EntityConfig } from './state';
import { createSelector } from '@ngrx/store';
import { filter, map, take } from 'rxjs/operators';
import { pipe } from 'rxjs';

export const crudFactory = <T>(name: string, config: EntityConfig<T>) => {
  return {
    selectEntities: (state: any) => {
      if (!state) { return []; }
      const entity = state.crudState.entities[name];
      if (entity) {
        const selector = entity.adapter.getSelectors().selectEntities;
        return selector(entity.entityState);
      } else {
        return [];
      }
    },

    selectAll: createSelector(
      (state: any) => state.crudState.entities[name],
      (entity: any) => {
        if (entity) {
          const selector = entity.adapter.getSelectors().selectAll;
          return selector(entity.entityState);
        } else {
          return [];
        }
      }
    ),

    loadRequestAction: () => new LoadRequestAction(name),
    createRequestAction: (entity: T) => new CreateRequestAction({name, entity}),
    deleteRequestAction: (entity: T) => new DeleteRequestAction({name, entity}),
    types: ActionTypes
  };
};

/**
 * Creates an pipable function witch:
 * - emits one value when a CreateSuccessAction matching each CreateRequestActions have passed the stream.
 * - the emitted value is an array with the created entities.
 * - after this the observable is completed.
 * @param requestActions - an array of CreateRequestActions
 */
export const awaitRequestCompletion = (requestActions: CreateRequestAction[]) => {
  const result = [];
  return pipe(
    filter((action: ActionsUnion, index) => {
      switch (action.type) {
        case (ActionTypes.CREATE_SUCCESS):
          if (requestActions.includes(action.payload.source)) {
            result.push(action.payload.entity);
          }
          break;
        case (ActionTypes.CREATE_FAILURE):
          if (requestActions.includes(action.payload.source)) {
            throw(this);
          }
      }
      return result.length === requestActions.length;
    }),
    take(1),
    map(_ => result)
  );
};
