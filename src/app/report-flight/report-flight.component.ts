import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormBuilder, RequiredValidator, ValidatorFn, AbstractControl, FormGroup, ValidationErrors, FormArray } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { Person, Flight } from '../models';
import { mergeMap } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { Actions } from '@ngrx/effects';

import { ActionsUnion, awaitRequestCompletion, CreateSucccessAction, CreateRequestAction } from '../crud-store';

import { peopleStore, flightsStore, transactionsStore, membersStore } from '../app-store-model';
import { personValidator } from '../select-person/select-person.component';
import { TowFeesComponent } from './tow-fees/tow-fees.component';
import { TrikeFeeComponent } from './trike-fee/trike-fee.component';
import { FlightTypes } from '../flight-types.enum';
import { MatSnackBar } from '@angular/material';

enum FormState {'edit', 'submiting', 'success', 'fail'}

@Component({
  selector: 'sdfk-report-flight',
  templateUrl: './report-flight.component.html',
  styleUrls: ['./report-flight.component.scss']
})
export class ReportFlightComponent implements OnInit, OnDestroy {
  // TODO move to a global place
  public flightTypes = [FlightTypes.trikeflygningPrivat, FlightTypes.bogsering];
  readonly price = {freeFlight: 630, towing: 16 * 60, dayFee: 100};

  @ViewChild(TowFeesComponent) towFeeComponent: TowFeesComponent;
  @ViewChild(TrikeFeeComponent) trikeFeeComponent: TrikeFeeComponent;
  FormState = FormState;
  formState = FormState.edit;

  flightForm = this.fb.group({
    date: [new Date(), RequiredValidator],
    tackoStart: [null, RequiredValidator],
    tackoEnd: [null, RequiredValidator],
    trikePilot: [null, [RequiredValidator, personValidator]],
    type: ['bogsering', RequiredValidator],
    note: null
  }, { validators: [tackoValidator] });
  // charge private trike flight, create here to ensure data is preserved when <TrikeFeeComponent> is hidden/destroyed
  towFeeForm = this.fb.group({
    hgPilots: this.fb.array([])
  });
  // charge aerotowing, array of towed pilots, create here to ensure data is preserved when <TowFeeComponent> is hidden/destroyed
  trikeFeeForm = TrikeFeeComponent.generateForm(this.fb);

  members$;
  people$: Observable<Person[]>;

  subscription: Subscription[] = [];

  constructor(
    private store$: Store<any>,
    private fb: FormBuilder,
    private actions$: Actions<ActionsUnion>,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.members$ = this.store$.pipe(select(membersStore.selectEntities));
    this.people$ = this.store$.pipe(select(peopleStore.selectAll));
  }

  ngOnDestroy(): void {
    this.subscription.forEach(sub => sub.unsubscribe());
    this.subscription = [];
  }

  getTowCost() {
    let result = null;
    if (this.flightForm.value.tackoEnd && this.flightForm.value.tackoStart) {
      result = (this.flightForm.value.tackoEnd - this.flightForm.value.tackoStart) * this.price.towing;
    }
    return result;
  }

  onSubmit() {
    let feeComponent;
    switch (this.flightForm.value.type) {
      case (FlightTypes.trikeflygningPrivat):
        feeComponent = this.trikeFeeComponent;
        break;
      case (FlightTypes.bogsering):
        feeComponent = this.towFeeComponent;
        break;
      default:
        throw new Error(this.flightForm.value.type + ' type of flight not supported in billing');
    }

    if (!this.flightForm.valid || !feeComponent.form.valid) {
      markAsTouched(this.flightForm);  // mark trike pilot as touched
      markAsTouched(feeComponent.form);
      return false;
    }
    this.formState = FormState.submiting;
    const createFlightRequest = this.sendCreateFlightRequest();
    const subscription = this.actions$.pipe(
      awaitRequestCompletion([createFlightRequest]),
      mergeMap((flights: Flight[]) => {
        const createFeeActions = feeComponent.chargeFlight(flights[0]);
        createFeeActions.forEach(createFee => {
          this.store$.dispatch(createFee);
        });
        return this.actions$.pipe(
          awaitRequestCompletion(createFeeActions),
        );
      }),
    ).subscribe(
      _ => {
        this.formState = FormState.success;
        this.snackBar.open('Flyget har registrerats', '', {
          duration: 4000,
        });
        this.resetForm();
        subscription.unsubscribe();
      },
      error => {
        console.error('error ' + error);
        this.formState = FormState.fail;
        subscription.unsubscribe();
      },
      () => {
        console.log('complete!');
        subscription.unsubscribe();
      }
    );
  }

  /**
   * Dispatch a CreateRequestAction to the store.
   * @return the CreateRequestAction object.
   */
  private sendCreateFlightRequest(): CreateRequestAction {
    const flightData = {
      flightId: null,
      date: this.flightForm.value.date.toLocaleDateString('sv'),
      tackoStart: +this.flightForm.value.tackoStart,
      tackoEnd: +this.flightForm.value.tackoEnd,
      licNr: this.flightForm.value.trikePilot.licNr,
      type: this.flightForm.value.type,
      note: this.flightForm.value.note,
      trike: 'SE-VTC'
    };
    const createRequest = flightsStore.createRequestAction(flightData);
    this.store$.dispatch(createRequest);
    return createRequest;
  }

  resetForm() {
    this.flightForm.reset();
    this.trikeFeeForm.reset();
    this.towFeeForm.reset();
    this.flightForm.get('date').setValue(new Date());
    this.flightForm.get('type').setValue('bogsering');
    this.trikeFeeForm.get('trikePilotPay').setValue(true);
    this.formState = FormState.edit;
  }

  getSubmitText() {
    switch (this.formState) {
      case (FormState.edit):
        return 'skicka in';
      case (FormState.submiting):
        return 'bearbetar';
      case (FormState.success):
        return 'sparat';
      case (FormState.fail):
        return 'misslyckades';
    }
  }
}

const tackoValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  let error = null;
  const tackoStart = control.get('tackoStart');
  const tackoEnd = control.get('tackoEnd');
  if (!tackoStart.value || isNaN(+tackoStart.value)) {
    error = { tacko: 'ange ett tal' };
    tackoStart.setErrors(error);
  }
  if (!tackoEnd.value || isNaN(+tackoEnd.value)) {
    error = { tacko: 'ange ett tal' };
    tackoEnd.setErrors(error);
  }
  if (!error) {
    if (+tackoStart.value >= +tackoEnd.value) {
      error = { tacko: 'start måste ske före landning' };
    }
    // clear error status i both form controls if valid
    tackoEnd.setErrors(error);
    tackoStart.setErrors(error);
  }
  return error;
};

  /**
   * Marks all controls in a form group as touched
   * This makes shure parts of the form beloning to subcomponentes are marked as touched when errors are detected during form submission.
   * @param formGroup - The form group to touch
   */
const markAsTouched = (ctrl: AbstractControl) => {
  ctrl.markAsTouched();
  if (ctrl instanceof FormArray) {
    ctrl.controls.forEach(child => markAsTouched(child));
  } else if (ctrl instanceof FormGroup) {
    Object.keys(ctrl.controls).forEach(child => markAsTouched(ctrl.controls[child]));
  }
};
