import { Injectable, Inject } from '@angular/core';
import { Actions, Effect, ofType, OnInitEffects } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { mergeMap, map, catchError } from 'rxjs/operators';
import * as crudActions from './actions';
import { CrudConfigService } from './state';
import { CrudService } from './crud.service';

@Injectable({
  providedIn: 'root',
})
export class CrudStateEffects implements OnInitEffects {
  constructor(
    private actions$: Actions<crudActions.ActionsUnion>,
    private dataSource: CrudService<any, any>,
    @Inject(CrudConfigService) private config) {
  }

  // --- Config ---
  @Effect()
  configEffect$: Observable<crudActions.ActionsUnion> = this.actions$.pipe(
    ofType(crudActions.ActionTypes.SET_CONFIG),
    mergeMap(action => {
      const initActions = [];
      Object.keys(action.payload).forEach(key => {
        if (action.payload[key].autoload) {
          initActions.push(new crudActions.LoadRequestAction(key));
        }
      });
      return of(...initActions);
    })
  );

  // --- Load ---
  @Effect()
  loadEntitiesEffect$: Observable<Action> = this.actions$.pipe(
    ofType(crudActions.ActionTypes.LOAD_REQUEST),
    mergeMap(action => {
      const name = action.payload;
      return this.dataSource.load(this.config[name].endpoint).pipe(
        map(data => new crudActions.LoadSucccessAction({name, data, source: action})),
        catchError(error => of(new crudActions.LoadFailureAction({error, source: action})))
      );
    }),
  );

  // --- Create ---
  @Effect()
  createEffect$: Observable<Action> = this.actions$.pipe(
    ofType(crudActions.ActionTypes.CREATE_REQUEST),
    mergeMap(action => {
      const name = action.payload.name;
      const entity = action.payload.entity;
      return this.dataSource.create(entity, this.config[name].endpoint).pipe(
        map(data => new crudActions.CreateSucccessAction({name, entity: data[0], source: action})),
        catchError(error => of(new crudActions.CreateFailureAction({error, source: action})))
      );
    }),
  );

  // --- Delete ---
  @Effect()
  deleteEffect$: Observable<Action> = this.actions$.pipe(
    ofType(crudActions.ActionTypes.DELETE_REQUEST),
    mergeMap(action => {
      const name = action.payload.name;
      const entity = action.payload.entity;
      return this.dataSource.delete(entity, this.config[name].endpoint).pipe(
        map(data => new crudActions.DeleteSucccessAction({name, entity, source: action})),
        catchError(error => of(new crudActions.DeleteFailureAction({error, source: action})))
      );
    }),
  );

  ngrxOnInitEffects(): Action {
    return new crudActions.ConfigAction(this.config);
  }

}
