import { Action } from '@ngrx/store';
import { CrudConfig } from './state';

export enum ActionTypes {
  SET_CONFIG = '@holmby/CRUD-store/set-config/',

  LOAD_REQUEST = '@holmby/CRUD-store/load-request/',
  LOAD_FAILURE = '@holmby/CRUD-store/load-failure/',
  LOAD_SUCCESS = '@holmby/CRUD-store/load-success/',

  CREATE_REQUEST = '@holmby/CRUD-store/create-request/',
  CREATE_FAILURE = '@holmby/CRUD-store/create-failure/',
  CREATE_SUCCESS = '@holmby/CRUD-store/create-success/',

  DELETE_REQUEST = '@holmby/CRUD-store/delete-request/',
  DELETE_FAILURE = '@holmby/CRUD-store/delete-failure/',
  DELETE_SUCCESS = '@holmby/CRUD-store/delete-success/'
}

export class ConfigAction implements Action {
  readonly type = ActionTypes.SET_CONFIG;
  constructor(public payload: CrudConfig) { }
}

// --- Load -------------------------------------
// payload is the entity name
export class LoadRequestAction implements Action {
  readonly type = ActionTypes.LOAD_REQUEST;
  constructor(public payload: string) { }
}

// payload is the entity name and an array of loaded entities.
export class LoadSucccessAction implements Action {
  readonly type = ActionTypes.LOAD_SUCCESS;
  constructor(public payload: {name: string, data: any[], source: LoadRequestAction}) { }
}

// payload is an Error object
export class LoadFailureAction implements Action {
  readonly type = ActionTypes.LOAD_FAILURE;
  constructor(public payload: {error: Error, source: LoadRequestAction} ) { }
}

// --- Create -----------------------------------
// payload is the entity name, and the entity to create
export class CreateRequestAction implements Action {
  readonly type = ActionTypes.CREATE_REQUEST;
  constructor(public payload: {name: string, entity: any} ) { }
}

// payload is the entity name and the created entity.
export class CreateSucccessAction implements Action {
  readonly type = ActionTypes.CREATE_SUCCESS;
  constructor(public payload: {name: string, entity: any, source: CreateRequestAction}) { }
}

// payload is an Error object
export class CreateFailureAction implements Action {
  readonly type = ActionTypes.CREATE_FAILURE;
  constructor(public payload: {error: Error, source: CreateRequestAction}) { }
}

// --- Delete -----------------------------------
// payload is the entity name
export class DeleteRequestAction implements Action {
  readonly type = ActionTypes.DELETE_REQUEST;
  constructor(public payload: {name: string, entity: any} ) { }
}

// payload is the entity name and the deleted entity.
export class DeleteSucccessAction implements Action {
  readonly type = ActionTypes.DELETE_SUCCESS;
  constructor(public payload: {name: string, entity: any, source: DeleteRequestAction}) { }
}

// payload is an Error object
export class DeleteFailureAction implements Action {
  readonly type = ActionTypes.DELETE_FAILURE;
  constructor(public payload: {error: Error, source: DeleteRequestAction}) { }
}

export type ActionsUnion = ConfigAction |
                          LoadRequestAction | LoadSucccessAction | LoadFailureAction |
                          CreateRequestAction | CreateSucccessAction | CreateFailureAction |
                          DeleteRequestAction | DeleteSucccessAction | DeleteFailureAction;
