import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreRouterConnectingModule, routerReducer } from '@ngrx/router-store';

import { environment } from 'src/environments/environment.prod';
import { CrudStoreModule } from './crud-store/';
import { AppEffects } from './app.effects';
import { crudConfig } from './app-store-model';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forRoot({
      router: routerReducer
    }),
    EffectsModule.forRoot([AppEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
    StoreRouterConnectingModule.forRoot(),

    CrudStoreModule.forRoot(crudConfig),
  ],
  declarations: []
})
export class AppStoreModule {}

