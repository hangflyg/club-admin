import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { crudReducer } from './reducer';
import { CrudConfig, CrudConfigService } from './state';
import { CrudStateEffects } from './effects';
import { CrudService } from './crud.service';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('crudState', crudReducer),
    EffectsModule.forFeature([CrudStateEffects])
  ],
  providers: [CrudStateEffects, CrudService]
})
export class RootCrudStoreModule {}

export class CrudStoreModule {
  static forRoot(config: CrudConfig): ModuleWithProviders<CrudStoreModule> {
    return {
      ngModule: RootCrudStoreModule,
      providers: [
                  { provide: CrudConfigService, useValue: config },
                ]
    };
  }
}
