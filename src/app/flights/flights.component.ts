import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

import { Flight, fillBlanks, WithPilotName, addPilotName, compareFlights } from '../models';
import { selectError, selectRequestCount } from '../crud-store';
import { peopleStore, flightsStore } from '../app-store-model';

@Component({
  selector: 'sdfk-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.scss'],
})
export class FlightsComponent implements OnInit, OnDestroy {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['date', 'taco-start', 'taco-end', 'type', 'pilot', 'delete'];
  flightsSource = new MatTableDataSource<Flight&WithPilotName>([]);
  dataAccessMap = {
    date: (flight) => flight.date,
    'taco-start': (flight) => flight.tackoStart,
    'taco-end': (flight) => flight.tackoEnd,
    type: (flight) => flight.type,
    pilot: (flight) => flight.licNr,
    delete: (transaction) => 0                       // dummy, to avoid errors
  };

  subscription: Subscription;
  requestCount$: Observable<boolean>;
  error$: Observable<any>;

  constructor(
    private store$: Store<any>
  ) {}

  ngOnInit() {
    this.flightsSource.sort = this.sort;
    this.flightsSource.paginator = this.paginator;
    this.flightsSource.filterPredicate = (flight: Flight, filter: string) => flight.date >= filter;
    this.flightsSource.sortingDataAccessor = (data: Flight, sortHeaderId: string) => this.dataAccessMap[sortHeaderId](data);
    this.error$ = this.store$.pipe(select(selectError));
    this.requestCount$ = this.store$.pipe(select(selectRequestCount));

    const people$ = this.store$.pipe(select(peopleStore.selectEntities));
    const flights$ = this.store$.pipe(
      select(flightsStore.selectAll),
      map(list => list.sort((a, b) => compareFlights(a, b))),
      fillBlanks(),
      addPilotName(people$)
    );

    this.subscription = flights$.subscribe(flights => this.flightsSource.data = flights);
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
  }

  afterDate($event) {
    this.flightsSource.filter = $event.value.toLocaleDateString('sv');
  }
  beforeDate($event) {
    alert('TODO');
    this.flightsSource.filter = $event.value.toLocaleDateString('sv');
  }
  deleteFlight(flight) {
    this.store$.dispatch(flightsStore.deleteRequestAction(flight));
  }
}
