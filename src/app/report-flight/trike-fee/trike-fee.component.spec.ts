import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrikeFeeComponent } from './trike-fee.component';

describe('TrikeFeeComponent', () => {
  let component: TrikeFeeComponent;
  let fixture: ComponentFixture<TrikeFeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrikeFeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrikeFeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
