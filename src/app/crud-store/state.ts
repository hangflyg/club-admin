import { EntityState } from '@ngrx/entity';
import { InjectionToken } from '@angular/core';
import { IdSelector, Comparer, EntityAdapter } from '@ngrx/entity/src/models';

export const CrudConfigService = new InjectionToken<CrudConfig>('CrudConfig');

// --- Config ------------------------------------------------------
export interface EntityConfig<T> {
  adapterOptions: {
    selectId?: IdSelector<T>;
    sortComparer?: false | Comparer<T>;
  };
  endpoint: string;
  autoload: boolean;
}

export interface CrudConfig {
  [key: string]: EntityConfig<any>;
}

// --- State --------------------------------------------------------
export interface CrudRootState {
  'crudStore': CrudState;
}

export interface CrudState {
  config: CrudConfig;
  requestCount: number;
  error: Error;
  entities: EntityMap<any>;
}

export interface EntityMap<T> {
  [key: string]: {
    adapter: EntityAdapter<T>;
    entityState: EntityState<T>;
  };
}

export const initialState: CrudState = {
  config: null,
  requestCount: 0,
  error: null,
  entities: {}
};
