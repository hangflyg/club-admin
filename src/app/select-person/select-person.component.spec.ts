import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SdfkSelectPersonComponent } from './sdfk-select-person.component';

describe('SdfkSelectPersonComponent', () => {
  let component: SdfkSelectPersonComponent;
  let fixture: ComponentFixture<SdfkSelectPersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SdfkSelectPersonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SdfkSelectPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
