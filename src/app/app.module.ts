import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, Injectable, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import localeSv from '@angular/common/locales/sv';
import { ReactiveFormsModule } from '@angular/forms';
import { Platform } from '@angular/cdk/platform';
import { LayoutModule } from '@angular/cdk/layout';

import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  NativeDateAdapter,
  DateAdapter,
} from '@angular/material';

import { AuthModule, AuthConfig } from '@holmby/auth';

import { AppRoutingModule } from './app-routing.module';
import { AppStoreModule } from './app-store.module';
import { AppComponent } from './app.component';
import { BalanceComponent } from './balance/balance.component';
import { FlightsComponent } from './flights/flights.component';
import { NavigationComponent } from './navigation/navigation.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ReportFlightComponent } from './report-flight/report-flight.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { SelectPersonComponent } from './select-person/select-person.component';
import { TowFeesComponent } from './report-flight/tow-fees/tow-fees.component';
import { TrikeFeeComponent } from './report-flight/trike-fee/trike-fee.component';

registerLocaleData(localeSv, 'sv');

@Injectable()
export class SwedishDateAdapter extends NativeDateAdapter {
  constructor() {
    super('sv', new Platform());
  }

  getFirstDayOfWeek(): number {
    return 1;
  }
}

const authConfig: AuthConfig = {
  persistent: false,               // store login info in localstore (restore login state when reloading page)

  inactivWarningTime: 25 * 60 * 1000,    // after inactivWarningTime miliseconds of inactivity a warning is shown
  inactivLogoutTime: 30 * 60 * 1000,     // after inactivLogoutTime miliseconds of inactivity the user is logged out

  text: {
    loginUser: 'lösenord',
    loginPassword: 'lämna tom'
  },

  url: {
    login: '/rest/sdfk/v1/login.sdfk.php',
    forgotPassword: '/rest/holmby/v1/forgotPassword.php',
  }
};


@NgModule({
  declarations: [
    AppComponent,
    BalanceComponent,
    FlightsComponent,
    NavigationComponent,
    PageNotFoundComponent,
    ReportFlightComponent,
    SelectPersonComponent,
    TransactionsComponent,
    TowFeesComponent,
    TrikeFeeComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    LayoutModule,
    MatNativeDateModule,
    ReactiveFormsModule,

    MatAutocompleteModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatPaginatorModule,
    MatSelectModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,

    AuthModule.forRoot(authConfig),

    AppRoutingModule,
    AppStoreModule
  ],
  providers: [
    MatNativeDateModule,
    {provide: DateAdapter, useClass: SwedishDateAdapter},  // make monday first day of week
    { provide: LOCALE_ID, useValue: 'sv' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
