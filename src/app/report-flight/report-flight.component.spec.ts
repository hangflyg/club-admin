import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportFlightComponent } from './report-flight.component';

describe('ReportFlightComponent', () => {
  let component: ReportFlightComponent;
  let fixture: ComponentFixture<ReportFlightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportFlightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportFlightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
