import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TowFeesComponent } from './tow-fees.component';

describe('TowFeesComponent', () => {
  let component: TowFeesComponent;
  let fixture: ComponentFixture<TowFeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TowFeesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TowFeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
