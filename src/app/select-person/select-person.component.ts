import { Component, OnInit, Input } from '@angular/core';
import { Person } from '../models';
import { Observable } from 'rxjs';
import { FormGroup, ValidatorFn, ValidationErrors } from '@angular/forms';
import { map } from 'rxjs/operators';

@Component({
  selector: 'sdfk-select-person',
  templateUrl: './select-person.component.html',
  styleUrls: ['./select-person.component.scss']
})
export class SelectPersonComponent implements OnInit {
  @Input() required: boolean;
  @Input() placeholder: string;
  @Input() form: FormGroup;
  @Input() key: string;
  @Input() people: Person[];
  filteredPeople: Observable<Person[]>;

  constructor(
    ) { }

  ngOnInit() {
    this.filteredPeople = this.form.controls[this.key].valueChanges
      .pipe(
        map(value => typeof value === 'string' ? value : this.displayFn(value)),
        map(name => this._filter(name))
      );
  }

  getErrorMessage() {
    const errors = this.form.controls[this.key].errors;
    if (errors) {
      if (errors.required) {
        return 'obkigatorisk';
      } else if (errors.person) {
        return errors.person;
      } else {
        const messages = Object.values(errors);
        return messages[0];
      }
    }
    return '';
  }

  private _filter(name: string): Person[] {
    const filterValue = name ? name.toLowerCase() : '';
    return this.people.filter(person => this.displayFn(person).toLowerCase().indexOf(filterValue) >= 0);
  }

  displayFn(person?: Person): string | undefined {
    return person ? (person.firstName || '') + ' ' + (person.lastName || '') : undefined;
  }

}

/**
 *
 * @param control Validate that the field is empty, or contains a Person object.
 */
export const personValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  if (!control.value) { return null; }
  const isError = typeof control.value === 'string';
  return isError ? {person: 'välj person från listan'} : null;
};
