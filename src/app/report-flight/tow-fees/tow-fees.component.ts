import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, ValidatorFn, ValidationErrors, FormControl } from '@angular/forms';
import { Person, memberPrimaryKey, Flight } from 'src/app/models';
import { personValidator } from 'src/app/select-person/select-person.component';
import { FlightTypes } from 'src/app/flight-types.enum';
import { transactionsStore } from 'src/app/app-store-model';
import { CreateRequestAction } from 'src/app/crud-store';

interface Row {
  control: FormGroup;
  price: number;
  dayFee: number;
}

@Component({
  selector: 'sdfk-tow-fees',
  templateUrl: './tow-fees.component.html',
  styleUrls: ['./tow-fees.component.scss']
})
export class TowFeesComponent implements OnInit, OnDestroy {
  @Input() set towCost(totalPrice: number) {
    this.totalPrice = totalPrice;
    const value = this.formArray ? this.formArray.value : [];
    this.onValueChange(value);
  }
  @Input() form: FormArray;
  @Input() people: Person[];
  @Input() members;
  @Input() dayFee: number;
  @Input() set date(date: Date) {
    this.year = date ? date.getFullYear() : null;
    const value = this.formArray ? this.formArray.value : [];
    this.onValueChange(value);  // update day fee when year changes
  }
  formArray: FormArray;
  year: number;
  totalPrice: number;
  totalTowHeight = 0;
  rows = [];
  private subscription;

  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.formArray = this.form.get('hgPilots') as FormArray;
    // if an old form exists, build the internal structure.
    this.formArray.controls.forEach(control =>
      this.rows.push({
        control,
        price: null,
        dayFee: null,
      })
    );
    this.onValueChange(this.formArray.value);
    if (this.formArray.length === 0) {
      this.addTowRow();
      this.addTowRow();
      this.addTowRow();
    }
    this.subscription = this.formArray.valueChanges.subscribe(value => this.onValueChange(value));
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
  }

  onValueChange(value) {
    this.totalTowHeight = 0;
    value.forEach(row => {
      const height = +row.towHeight;
      if (height) {
        this.totalTowHeight += height;
      }
    });
    value.forEach((row, index) => {
      // height price
      const price = Math.round((+row.towHeight) / this.totalTowHeight * this.totalPrice);
      if (price) {
        this.rows[index].price = price;
      } else {
        this.rows[index].price = null;
      }
      // day fee
      const pilot = row.hgPilot;
      if (pilot && pilot.licNr) {
        this.rows[index].dayFee = this.payDayFee(pilot.licNr) ? this.dayFee : 0;
      } else {
        this.rows[index].dayFee = null;
      }
    });
  }

  payDayFee(licNr): boolean {
    const memberObj = {memberId: null, licNr, year: this.year, level: null};
    const key = memberPrimaryKey(memberObj);
    return !(this.members[key] && this.members[key].level === 'bogser');
    // TODO, check if pilot have payed day fee for this day
  }

  addTowRow() {
    const control = this.fb.group({
      hgPilot: [null, personValidator],
      towHeight: [null],
    }, { validators: rowValidator});
    // add to rows first, when adding a control, onValueChange() will be called directly
    this.rows.push({
      control,
      price: null,
      dayFee: null,
    });
    this.formArray.push(control);
    return false;
  }

  getErrorMessage(i: number): string {
    const errors = this.formArray.controls[i].errors;
    if (errors) {
      if (errors.required) {
        return 'obkigatorisk';
      } else {
        const messages = Object.values(errors);
        return messages[0];
      }
    }
    return '';
  }

  chargeFlight(newFlight: Flight): CreateRequestAction[] {
    const transactions = [];
    this.rows.forEach(row => {
      if (row.control.value.hgPilot) {
        const transData = {
          transactionId: null,
          licNr: row.control.value.hgPilot.licNr,
          flightId: newFlight.flightId,
          date: newFlight.date,
          height: row.control.value.towHeight,
          dayFee: row.dayFee,
          price: row.dayFee + row.price,
          type: FlightTypes.bogsering,
          note: null
        };
        const transaction = transactionsStore.createRequestAction(transData);
        transactions.push(transaction);
      }
    });
    return transactions;
  }
}

export const rowValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  let error = null;
  const value = control.value;
  if ((value.towHeight) && !Number.isInteger(+value.towHeight)) {
    // tow hight must be an integer
    error = {towHeight: 'ange ett heltal'};
    control.get('towHeight').setErrors(error);
  } else if (value.towHeight && (!value.hgPilot)) {
    // if the tow height is given, the pilot is required
    error = {hgPilot: 'ange en pilot'};
    control.get('hgPilot').setErrors(error);
  } else if ((!value.towHeight) && value.hgPilot) {
    // if a pilot is geven, tow height is required
    error = {towHeight: 'ange bogserhöjd'};
    control.get('towHeight').setErrors(error);
  }
  return error;
};
