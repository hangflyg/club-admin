import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';

import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';

import { selectError, selectRequestCount } from '../crud-store';

import { Person, Transaction, WithPilotName, addPilotName, compareTransactions } from '../models';
import { peopleStore, transactionsStore } from '../app-store-model';
import { personValidator } from '../select-person/select-person.component';

@Component({
  selector: 'sdfk-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit, OnDestroy {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['date', 'height', 'day-fee', 'price', 'type', 'pilot', 'note', 'delete'];
  transactionsSource = new MatTableDataSource<Transaction&WithPilotName>([]);
  dataAccessMap = {
    date: (transaction) => transaction.date,
    height: (transaction) => transaction.height,
    'day-fee': (transaction) => transaction.dayFee,
    price: (transaction) => transaction.price,
    type: (transaction) => transaction.type,
    pilot: (transaction) => transaction.pilotName,
    note: (transaction) => transaction.note,
    delete: (transaction) => 0                       // dummy, to avoid errors
  };


  filterForm = this.fb.group({
    startDate: null,
    endDate: null,
    person: [null, [personValidator]],
  });

  subscription: Subscription;
  requestCount$: Observable<boolean>;
  error$: Observable<any>;
  people$: Observable<Person[]>;

  constructor(
    private store$: Store<any>,
    private fb: FormBuilder
  ) {
    this.people$ = this.store$.pipe(select(peopleStore.selectAll));
  }

  ngOnInit() {
    // trigger recompute the filter function when the form changes
    this.filterForm.valueChanges.subscribe( () => {
      this.transactionsSource.filter = this.transactionsSource.filter + 1;
    });
    this.transactionsSource.filter = '0';
    this.transactionsSource.filterPredicate = (transaction: Transaction, filter: string) => {
      const filterPerson = this.filterForm.value.person;
      return !filterPerson || transaction.licNr == filterPerson.licNr;
    };
    this.transactionsSource.sort = this.sort;
    this.transactionsSource.paginator = this.paginator;
    this.transactionsSource.sortingDataAccessor = (data: Transaction, sortHeaderId: string) => this.dataAccessMap[sortHeaderId](data);
    this.error$ = this.store$.pipe(select(selectError));
    this.requestCount$ = this.store$.pipe(select(selectRequestCount));

    const peopleEntities$ = this.store$.pipe(select(peopleStore.selectEntities));

    const transactions = this.store$.pipe(
      select(transactionsStore.selectAll),
      map(list => list.sort((a, b) => compareTransactions(a, b))),
      addPilotName(peopleEntities$)
    );

    this.subscription = transactions.subscribe((transaction: any) => this.transactionsSource.data = transaction);

  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
  }
  afterDate($event) {
    this.transactionsSource.filter = $event.value.toLocaleDateString('sv');
  }
  beforeDate($event) {
    alert('TODO');
    this.transactionsSource.filter = $event.value.toLocaleDateString('sv');
  }
  deleteTransaction(transaction) {
    this.store$.dispatch(transactionsStore.deleteRequestAction(transaction));
  }

  onSubmit() {
    return false;
  }

}
