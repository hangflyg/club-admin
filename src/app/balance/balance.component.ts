import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Transaction, compareTransactions, WithPilotName, addPilotName } from '../models';
import { Store, select } from '@ngrx/store';
import { peopleStore, transactionsStore } from '../app-store-model';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';

interface Balance {
  balance: number;
  licNr: number;
}

@Component({
  selector: 'sdfk-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.scss']
})
export class BalanceComponent implements OnInit, OnDestroy {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['pilot', 'balance'];
  balancesSource = new MatTableDataSource<Balance&WithPilotName>([]);
  dataAccessMap = {
    balance: (obj) => obj.balance,
    pilot: (obj) => obj.pilotName,
  };

  subscriptions: Subscription[] = [];

  constructor(private store$: Store<any>) {}

  ngOnInit() {
    this.balancesSource.sort = this.sort;
    this.balancesSource.paginator = this.paginator;
    this.balancesSource.filterPredicate = (balance: Balance, filter: string) => balance.balance >= +filter;
    this.balancesSource.sortingDataAccessor = (data: Balance, sortHeaderId: string) => this.dataAccessMap[sortHeaderId](data);

    const people$ = this.store$.pipe(select(peopleStore.selectEntities));

    const balances$ = this.store$.pipe(
      select(transactionsStore.selectAll),
      map(list => this.makeBalance(list)),
      addPilotName(people$),
    );

    this.subscriptions.push(
      balances$.subscribe((balances: any) => this.balancesSource.data = balances)
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subs => subs.unsubscribe());
    this.subscriptions = [];
  }

  makeBalance(transactions: Transaction[]): Balance[] {
    const myMap = new Map();
    transactions.forEach(transaction => {
      const balance = myMap.get(transaction.licNr) || 0;
      myMap.set(transaction.licNr, balance + transaction.price);
    });
    const result = [];
    myMap.forEach((balance, licNr) => {
      if (balance !== 0) {
        result.push({balance, licNr});
      }
    });
    return result;
  }
}
