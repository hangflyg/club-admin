import { EntityConfig } from './crud-store/state';
import { Flight, Person, personPrimaryKey, flightPrimaryKey, transactionPrimaryKey, Transaction, Member, memberPrimaryKey } from './models';
import { crudFactory } from './crud-store/';

const peopleEntityConfig: EntityConfig<Person> = {
  adapterOptions: { selectId: personPrimaryKey, sortComparer: false},
  endpoint: '/rest/sdfk/v1/people.php',
  autoload: false
};

const memberEntityConfig: EntityConfig<Member> = {
  adapterOptions: { selectId: memberPrimaryKey, sortComparer: false},
  endpoint: '/rest/sdfk/v1/members.php',
  autoload: true
};

const flightsEntityConfig: EntityConfig<Flight> = {
  adapterOptions: { selectId: flightPrimaryKey, sortComparer: false},
  endpoint: '/rest/sdfk/v1/flights.php',
  autoload: true
};

const transactionsEntityConfig: EntityConfig<Transaction> = {
  adapterOptions: { selectId: transactionPrimaryKey, sortComparer: false},
  endpoint: '/rest/sdfk/v1/transactions.php',
  autoload: true
};

export const crudConfig = {
  people: peopleEntityConfig,
  members: memberEntityConfig,
  flights: flightsEntityConfig,
  transactions: transactionsEntityConfig
};

export const peopleStore = crudFactory('people', peopleEntityConfig);
export const membersStore = crudFactory('members', memberEntityConfig);
export const flightsStore = crudFactory('flights', flightsEntityConfig);
export const transactionsStore = crudFactory('transactions', transactionsEntityConfig);

