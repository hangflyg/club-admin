import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CrudService<T, K extends keyof T> {
  constructor(
      public http: HttpClient
    ) { }

  load(url: string): Observable<T[]> {
    return this.http.get<T[]>(url);
  }

  delete(keys: K, url: string): Observable<number> {
    let params = new HttpParams();
    Object.keys(keys).forEach(name => {
      params = params.set(name, keys[name]);
    });
    return this.http.delete<number>(url, { params });
  }

  create(entry: T, url: string): Observable<T[]> {
    return this.http.post<T[]>(url, entry);
  }

}

