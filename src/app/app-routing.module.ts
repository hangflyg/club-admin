import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BalanceComponent } from './balance/balance.component';
import { FlightsComponent } from './flights/flights.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ReportFlightComponent } from './report-flight/report-flight.component';
import { TransactionsComponent } from './transactions/transactions.component';

const routes: Routes = [
  { path: 'flights', component: FlightsComponent },
  { path: 'transactions', component: TransactionsComponent },
  { path: 'report', component: ReportFlightComponent },
  { path: 'balance', component: BalanceComponent },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
