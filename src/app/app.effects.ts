import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
// import { CrudService } from './crud-store/crud.service';
import { AuthActionTypes } from '@holmby/auth';
import { peopleStore } from './app-store-model';
import { ActionsUnion } from './crud-store';

@Injectable({
  providedIn: 'root',
})
export class AppEffects {
  constructor(
    private actions$: Actions<Action>,
//    private dataSource: CrudService<any, any>
  ) {
  }

  // --- Config ---
  @Effect()
  configEffect$: Observable<ActionsUnion> = this.actions$.pipe(
    ofType(AuthActionTypes.LoginSuccess),
    map( _ => peopleStore.loadRequestAction())
  );

}
