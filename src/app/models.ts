import { Observable, combineLatest } from 'rxjs';
import { EntityState } from '@ngrx/entity';
import { map } from 'rxjs/operators';

export interface Flight {
  flightId: number;
  date: string;
  tackoStart: number;
  tackoEnd: number;
  licNr: number;
  type: string;
  note: string;
  trike: string;
}

export interface Transaction {
  transactionId: number;
  licNr: number;
  flightId: number;
  date: string;
  height: number;
  dayFee: number;
  price: number;
  type: string;
  note: string;
}

export interface Person {
  licNr: number;
  firstName: string;
  lastName: string;
  street: string;
  postCode: string;
  city: string;
  country: string;
  phone: string;
  mobile: string;
  email: string;
  info: string;
}

export interface Member {
  memberId: number;
  licNr: number;
  year: number;
  level: string;
}

export function flightPrimaryKey(flight: Flight) { return flight.flightId; }
export function transactionPrimaryKey(transaction: Transaction) { return transaction.transactionId; }
export function personPrimaryKey(person: Person) { return person.licNr; }
export function memberPrimaryKey(member: Member) { return member.licNr + ':' + member.year; }

/**
 * Any object refering to a person
 */
export interface PilotRef {
  licNr: number;
}
/**
 * An object containg the pilot name as a string
 */
export interface WithPilotName {
  pilotName: string;
}

export function compareFlights(a: Flight, b: Flight) {
  if (a.trike === b.trike) {
    return a.tackoStart > b.tackoStart ? -1 : 1;
  }
  return a.trike > b.trike ? 1 : -1;
}

export function compareTransactions(a: Transaction, b: Transaction) {
  if (a.date > b.date) {
    return -1;
  }
  if (a.date < b.date) {
    return 1;
  }
  return a.transactionId > b.transactionId ? -1 : 1;
}

/**
 * A pipable function adding dummy flights to fill any gaps in tacko time for consecutive flights.
 * Enusres previousFlight.tackoEnd === nextFlight.tackoStart for all flights in the list.
 * The added dummy flights will have null value for flightId, date, towHeight, andlicNr.
 */
export const fillBlanks = () => (sourceStream: Observable<Flight[]>) =>
  sourceStream.pipe(map((src) => {
    let prev = null;
    const result = [];
    for (const flight of src) {
      if (prev && (prev.tackoStart !== flight.tackoEnd)) {
        result.push({
          flightId: null,
          date: null,
          towHeight: null,
          tackoStart: flight.tackoEnd,
          tackoEnd: prev.tackoStart,
          licNr: null,
          type: 'underlag saknas, tid: ' + Math.round((prev.tackoStart - flight.tackoEnd) * 100) / 100
        });
      }
      result.push(flight);
      prev = flight;
    }
    return result;
}));

/**
 * A pipable function.
 * Adds the pilot name as a string to all objects in observable stream.
 * The source observable is a stream of an array contining <PilotRef> objects
 * @param people an observable of EntitySets of people, Observable<EntityState<Person>>)
 */
export const addPilotName = (people: Observable<EntityState<Person>>) => <T extends PilotRef>(source: Observable<T[]>) =>
  combineLatest(source, people).pipe(
    map(([f, p]) => f.map( (row) => {
      const pilot = p[row.licNr];
      let name = '';
      if (pilot) {
        name = (pilot.firstName || '') + ' ' + (pilot.lastName || '');
      }
      return {...row, pilotName: name} as T & WithPilotName;
  }))
);

