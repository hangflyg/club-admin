import { createSelector } from '@ngrx/store';

/**
 * Expose all selectors from the EntityAdapter.
 * The selectors need a prop object naming the entity to select
 * prop: {entity: 'name of entity'}
 *
 * Example:
 *   constructor(store: Store<any>) {
 *     this.people = store.pipe(select(selectEntities, {entity: 'people'}));
 *   }
 */

const selectCrudState = (state: any) => state.crudState;
export const selectError = (state: any) => state.crudState.error;
export const selectRequestCount = (state: any) => state.crudState.requestCount;

export const selectTotal = createSelector(
  selectCrudState,
  (state: any, props) => {
    const entity = state.entities[props.entity];
    if (entity) {
      const selector = entity.adapter.getSelectors().selectTotal;
      return selector(entity.entityState);
    } else {
      return [];
    }
  }
);


export const selectAll = createSelector(
  selectCrudState,
  (state: any, props) => {
    const entity = state.entities[props.entity];
    if (entity) {
      const selector = entity.adapter.getSelectors().selectAll;
      return selector(entity.entityState);
    } else {
      return [];
    }
  }
);

export const selectIds = createSelector(
  selectCrudState,
  (state: any, props) => {
    const entity = state.entities[props.entity];
    if (entity) {
      const selector = entity.adapter.getSelectors().selectIds;
      return selector(entity.entityState);
    } else {
      return [];
    }
  }
);

export const selectEntities = createSelector(
  selectCrudState,
  (state: any, props) => {
    const entity = state.entities[props.entity];
    if (entity) {
      const selector = entity.adapter.getSelectors().selectEntities;
      return selector(entity.entityState);
    } else {
      return [];
    }
  }
);
